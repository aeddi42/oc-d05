type 'a ft_ref = {mutable value: 'a}

let return value = {
    value = value
}

let get x =
    x.value

let set x value =
    x.value <- value

let bind x func =
    let y = func x.value in
    return y.value
