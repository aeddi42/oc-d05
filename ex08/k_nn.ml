let examples_of_files path =
    let file = open_in path in
    let extract_ex str =
        let lst = ref [] in
        let split = ref "" in
        let rest = ref str in
        let dest = ref 0 in
        let endd = ref 0 in
        while String.length !rest > 2 do
            dest := String.index !rest ',';
            endd := String.rindex !rest ',';
            split := String.sub !rest 0 !dest;
            if String.length !rest > 1 then
                rest := String.sub !rest (!dest + 1) ((String.length !rest) - (!dest + 1)) ;
            if (String.contains !split '.') == true then
                lst := !lst @ [(float_of_string !split)];
        done;
        let arr = Array.of_list !lst in
        (arr, !rest)
    in
    let examples = ref [] in
    try
        while true do
            examples := !examples @ [(extract_ex (input_line file))]
        done;
        (!examples @ [])
    with | End_of_file -> close_in file; (!examples @ []) 


let k_nn k lst radar =
    let eu_dist x y =
        let result = ref 0.0 in
        for i = 0 to Array.length x -1 do
            result := !result +. ((x.(i) -. y.(i)) ** 2.0)
        done;
        sqrt !result
    in
    let sort_val k bests tmp =
        for i = 0 to k do
        done;
    in
    let bests = Array.make k (0.0, (-1)) in
    let tmp = ref 0.0 in
    for i = 0 to List.length lst - 1 do 
        tmp := eu_dist (fst (List.nth lst i)) (fst (radar));
        sort_val k bests !tmp
    done;
    let choice =
        let g = ref 0 in
        let b = ref 0 in
        let index = ref 0 in
        let current = ref "" in
        for i = 0 to Array.length bests - 1 do 
            index := snd (bests.(i));
            if !index <> -1 then
                begin
                    current := snd (List.nth lst !index);
                    if !current = "g" then incr g else incr b;
                end;
        done;
        if !g < !b then "b" else "g"
    in
    if choice = "g" then print_endline "The closest radar is good !"
    else if choice = "b" then print_endline "The closest radar is bad !"
    else print_endline "Something went wrong !"


let () =
    try
        let radars = examples_of_files "ionosphere.test.csv" in
        let rand = Random.self_init (); Random.int (List.length radars) in
        let radar = List.nth radars rand in
        one_nn radars radar
    with | e -> print_endline "Something went wrong !"
