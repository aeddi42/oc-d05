let eu_dist x y =
    let result = ref 0.0 in
    for i = 0 to Array.length x -1 do
        result := !result +. ((x.(i) -. y.(i)) ** 2.0)
    done;
    sqrt !result

let () =
    let x = Array.make 4 0.0 in
    let y = Array.make 4 0.0 in
    x.(0) <- 0.0;
    x.(1) <- 3.0;
    x.(2) <- 4.0;
    x.(3) <- 5.0;
    y.(0) <- 7.0;
    y.(1) <- 6.0;
    y.(2) <- 3.0;
    y.(3) <- (-1.0);
    print_float (eu_dist x y);
    print_char '\n'
