let get_file () =
    open_in (Sys.argv).(1)

let say jokes size =
    Random.self_init ();
    print_endline (jokes.(Random.int size))

let get_jokes size =
    let file = get_file () in
    let jokes = Array.make size "" in
    for i = 0 to size - 1 do
        jokes.(i) <- input_line file
    done;
    close_in file;
    jokes

let get_size () =
    let file = get_file () in
    let size = ref 0 in
    while input_line file <> "###" do
        incr size
    done;
    close_in file;
    !size

let () =
    try
        let size = get_size () in
        let jokes = get_jokes size in
        say jokes size
    with
    | e -> print_endline "Something went wrong !"
