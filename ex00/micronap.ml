let my_sleep () = Unix.sleep 1

let do_sleep time =
    for i = 1 to time do
        my_sleep ()
    done

let get_time () =
    int_of_string (Array.get Sys.argv 1)

let () =
    try do_sleep (get_time ()) with 
    | e   -> ()
