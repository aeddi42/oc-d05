let sum x y =
    x +. y

let () =
    print_float (sum 1.5 2.7);
    print_char '\n';
    print_float (sum 2.9 1.3);
    print_char '\n';
    print_float (sum 2.1 2.1);
    print_char '\n'
