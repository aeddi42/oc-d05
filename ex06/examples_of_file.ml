let examples_of_files path =
    try
        let file = open_in path in
        let extract_ex str =
            let lst = ref [] in
            let split = ref "" in
            let rest = ref str in
            let dest = ref 0 in
            let endd = ref 0 in
            while String.length !rest > 2 do
                dest := String.index !rest ',';
                endd := String.rindex !rest ',';
                split := String.sub !rest 0 !dest;
                if String.length !rest > 1 then
                    rest := String.sub !rest (!dest + 1) ((String.length !rest) - (!dest + 1)) ;
                if (String.contains !split '.') == true then
                    lst := !lst @ [(float_of_string !split)];
            done;
            let arr = Array.of_list !lst in
            (arr, !rest)
        in
        let examples = ref [] in
        try
            while true do
                examples := !examples @ [(extract_ex (input_line file))]
            done;
            (!examples @ [])
        with | End_of_file -> close_in file; (!examples @ []) 
    with | e -> print_endline "Something went wrong !"; 
                let a = Array.make 2 0.0 in
                ([(a, "")] @ [(a, "")])
